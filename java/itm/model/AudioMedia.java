package itm.model;

/*******************************************************************************
 This file is part of the ITM course 2017
 (c) University of Vienna 2009-2017
 *******************************************************************************/

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;

//import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.ParseException;

import java.text.SimpleDateFormat;

public class AudioMedia extends AbstractMedia {

	// ***************************************************************
	// Fill in your code here!
	// ***************************************************************
	protected String encoding;
	protected long duration;
	protected String author;
	protected String title;
	protected Date date;
	protected String comment;
	protected String album;
	protected String track;
	protected String composer;
	protected String genre;
	protected float frequency;
	protected int bitrate;
	protected int channels;



	/**
	 * Constructor.
	 */
	public AudioMedia() {
		super();
	}

	/**
	 * Constructor.
	 */
	public AudioMedia(File instance) {
		super(instance);
	}

	/* GET / SET methods */
	
	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding){
		this.encoding=encoding;
	}

	public long getDuration(){
		return duration;
	}
	public void setDuration(long duration){
		this.duration=duration;
	}
	public String getAuthor(){
		return author;
	}

	public void setAuthor(String author){
		this.author=author;
	}

	public String getTitle(){
		return title;
	}
	public void setTitle(String title){
		this.title=title;
	}

	public Date getDate(){
		return date;
	}

	public void setDate(String date){
		try{
		this.date = new SimpleDateFormat("yyyy").parse(date);
		}catch(java.text.ParseException e){
			System.out.println("Fehler beim parsen in AudioMedia"+ e.getMessage());
		}
	}
	public String getComment(){
		return comment;
	}

	public void setComment(String comment){
		this.comment=comment;
	}
	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(float frequency) {
		this.frequency = frequency;
	}

	public int getBitrate() {
		return bitrate;
	}

	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}

	public int getChannels() {
		return channels;
	}

	public void setChannels(int channels) {
		this.channels = channels;
	}
	// ***************************************************************
	// Fill in your code here!
	// ***************************************************************

	/* (de-)serialization */


	/**
	 * Serializes this object to the passed file.
	 * 
	 */
	@Override
	public StringBuffer serializeObject() throws IOException {
		StringWriter data = new StringWriter();
		PrintWriter out = new PrintWriter(data);
		out.println("type: audio");
		StringBuffer sup = super.serializeObject();
		out.print(sup);

		// ***************************************************************
		// Fill in your code here!
		// ***************************************************************
		out.println("encoding: " + this.encoding);
		out.println("author: " + this.author);
		out.println("title: " + this.title);
		out.println("comment: " + this.comment);
		out.println("album: " + this.album);
		out.println("track: " + this.track);
		out.println("composer: " + this.composer);
		out.println("genre: " + this.genre);
		out.println("frequency: " + this.frequency);
		out.println("bitrate: " + this.bitrate);
		out.println("channels: " + this.channels);
		
		try {
			out.println("date: " + new SimpleDateFormat("yyyy").format(this.date));
		} catch (Exception e) {
			// do nothing
		}


		return data.getBuffer();
	}

	/**
	 * Deserializes this object from the passed string buffer.
	 */
	@Override
	public void deserializeObject(String data) throws IOException {
		super.deserializeObject(data);

		StringReader sr = new StringReader(data);
		BufferedReader br = new BufferedReader(sr);
		String line = null;
		while ((line = br.readLine()) != null) {

			// ***************************************************************
			// Fill in your code here!
			// ***************************************************************
			
		}
	}

}
