package itm.model;

/*******************************************************************************
 This file is part of the ITM course 2017
 (c) University of Vienna 2009-2017
 *******************************************************************************/

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

public class VideoMedia extends AbstractMedia {

	// ***************************************************************
	// Fill in your code here!
	// ***************************************************************

	/* video format metadata */
	protected String videoCodec;
	protected String codecID ;
	protected String videoFrameRate; 
	protected long videoLength;
	protected int videoHeight;
	protected int videoWidth;

	/* audio format metadata */
	protected String audioCodec;
	protected String audiocodecID;
	protected long audioChannels; 
	protected int audioSampleRate;
	protected int audioBitRate;
	/**
	 * Constructor.
	 */
	public VideoMedia() {
		super();
	}

	/**
	 * Constructor.
	 */
	public VideoMedia(File instance) {
		super(instance);
	}

	/* GET / SET methods */
	public String getVideoCodec(){
		return videoCodec;
	}
	public void setVideoCodec(String videoCodec){
		this.videoCodec=videoCodec;
	}

	public String getCodecID(){
		return codecID;
	}
	public void setCodecID(String codecID){
		this.codecID=codecID;
	}
	public String getVideoFrameRate(){
		return videoFrameRate;
	}
	public void setVideoFrameRate(String videoFrameRate){
		this.videoFrameRate=videoFrameRate;
	}
	public long getVideoLength(){
		return videoLength;
	}
	public void setVideoLength(long videoLength){
		this.videoLength=videoLength;
	}
	public float getVideoHeight(){
		return videoHeight;
	}
	public void setVideoHeight(int videoHeight){
		this.videoHeight=videoHeight;
	}

	public float getVideoWidth(){
		return videoWidth;
	}
	public void setVideoWidth(int videoWidth){
		this.videoWidth=videoWidth;
	}

	//setter und getter for audio
	public String getAudioCodec(){
		return audioCodec;
	}
	public void setAudioCodec(String audioCodec){
		this.audioCodec=audioCodec;
	}
	public String getAudiocodecID(){
		return audiocodecID;
	}
	public void setAudiocodecID(String audiocodecID){
	this.audiocodecID=audiocodecID;
	}
	public long getAudioChannels(){
		return audioChannels;
	}
	public void setAudioChannels(long audioChannels){
		this.audioChannels=audioChannels;
	}
	public float getAudioSampleRate(){
		return audioSampleRate;
	}
	public void setAudioSampleRate(int audioSampleRate){
		this.audioSampleRate=audioSampleRate;
	}
	
	public float getAudioBitRate(){
		return audioBitRate;
	}
	public void setAudioBitRate(int audioBitRate){
		this.audioBitRate=audioBitRate;
	}
	// ***************************************************************
	// Fill in your code here!
	// ***************************************************************

	/* (de-)serialization */

	/**
	 * Serializes this object to the passed file.
	 * 
	 */
	@Override
	public StringBuffer serializeObject() throws IOException {
		StringWriter data = new StringWriter();
		PrintWriter out = new PrintWriter(data);
		out.println("type: video");
		StringBuffer sup = super.serializeObject();
		out.print(sup);

		/* video fields */
		out.println("videoCodec: " + this.videoCodec);
		out.println("CodecID: " + this.codecID);
		out.println("videoFrameRate: " + this.videoFrameRate);
		out.println("videoLength: " + this.videoLength);
		out.println("videoHeight: " + this.videoHeight);
		out.println("videoWidth: " + this.videoWidth);
		
		/* audio fields */
		out.println("audioCodec: " + this.audioCodec);
		out.println("audiocodecID: " + this.audiocodecID);
		out.println("audioChannels: " + this.audioChannels);
		out.println("audioSampleRate: " + this.audioSampleRate);
		out.println("audioBitRate: " + this.audioBitRate);

		// ***************************************************************
		// Fill in your code here!
		// ***************************************************************
		return data.getBuffer();
	}

	/**
	 * Deserializes this object from the passed string buffer.
	 */
	@Override
	public void deserializeObject(String data) throws IOException {
		super.deserializeObject(data);

		StringReader sr = new StringReader(data);
		BufferedReader br = new BufferedReader(sr);
		String line = null;
		while ((line = br.readLine()) != null) {

			/* video fields */
			// ***************************************************************
			// Fill in your code here!
			// ***************************************************************
		}
	}

}
