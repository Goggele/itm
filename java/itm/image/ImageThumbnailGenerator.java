package itm.image;

/*******************************************************************************
    This file is part of the ITM course 2017
    (c) University of Vienna 2009-2017
*******************************************************************************/

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
    This class converts images of various formats to PNG thumbnails files.
    It can be called with 3 parameters, an input filename/directory, an output directory and a compression quality parameter.
    It will read the input image(s), grayscale and scale it/them and convert it/them to a PNG file(s) that is/are written to the output directory.

    If the input file or the output directory do not exist, an exception is thrown.
*/
public class ImageThumbnailGenerator 
{

    /**
        Constructor.
    */
    public ImageThumbnailGenerator()
    {
    }

    /**
        Processes an image directory in a batch process.
        @param input a reference to the input image file
        @param output a reference to the output directory
        @param overwrite indicates whether existing thumbnails should be overwritten or not
        @return a list of the created files
    */
    public ArrayList<File> batchProcessImages( File input, File output, boolean overwrite ) throws IOException
    {
        if ( ! input.exists() ) {
            throw new IOException( "Input file " + input + " was not found!" );
        }
        if ( ! output.exists() ) {
            throw new IOException( "Output directory " + output + " not found!" );
        }
        if ( ! output.isDirectory() ) {
            throw new IOException( output + " is not a directory!" );
        }

        ArrayList<File> ret = new ArrayList<File>();

        if ( input.isDirectory() ) {
            File[] files = input.listFiles();
            for ( File f : files ) {
                try {
                    File result = processImage( f, output, overwrite );
                    System.out.println( "converted " + f + " to " + result );
                    ret.add( result );
                } catch ( Exception e0 ) {
                    System.err.println( "Error converting " + input + " : " + e0.toString() );
                }
            }
        } else {
            try {
                File result = processImage( input, output, overwrite );
                System.out.println( "converted " + input + " to " + result );
                ret.add( result );
            } catch ( Exception e0 ) {
                System.err.println( "Error converting " + input + " : " + e0.toString() );
            }
        } 
        return ret;
    }  

    /**
        Processes the passed input image and stores it to the output directory.
        This function should not do anything if the outputfile already exists and if the overwrite flag is set to false.
        @param input a reference to the input image file
        @param output a reference to the output directory
        @param dimx the width of the resulting thumbnail
        @param dimy the height of the resulting thumbnail
        @param overwrite indicates whether existing thumbnails should be overwritten or not
    */
    protected File processImage( File input, File output, boolean overwrite ) throws IOException, IllegalArgumentException
    {
        if ( ! input.exists() ) {
            throw new IOException( "Input file " + input + " was not found!" );
        }
        if ( input.isDirectory() ) {
            throw new IOException( "Input file " + input + " is a directory!" );
        }
        if ( ! output.exists() ) {
            throw new IOException( "Output directory " + output + " not found!" );
        }
        if ( ! output.isDirectory() ) {
            throw new IOException( output + " is not a directory!" );
        }

        // create outputfilename and check whether thumb already exists
        File outputFile = new File( output, input.getName() + ".thumb.png" );
        if ( outputFile.exists() ) {
            if ( ! overwrite ) {
                return outputFile;
            }
        }

        // ***************************************************************
        //  Fill in your code here!
        // ***************************************************************

        // load the input image    
        BufferedImage original = ImageIO.read(input);

        double width = original.getWidth();  
        double height = original.getHeight();

        double scale = 1.0;  //Factor to multiply width and height, to scale it down if necessary
        //The AffineTransform operations are "last-specified-first-applied"
        //So i made use of Switch/Case to get the proper order
        int value = 2;  // value for Switch/Case
        

        // add a watermark of your choice and paste it to the image
        // e.g. text or a graphic
        String watermark = "ASK 4 PERMISSION";
        Graphics2D originalG2D = original.createGraphics();
        Font font = new Font("Garamond", Font.BOLD, (int)width/10);
        originalG2D.setFont(font);
        originalG2D.drawString(watermark, 0, (int)height/4);
        originalG2D.dispose();


        //If width of image is bigger than 200px (or height, if profil-orientated), needs to be scaled
        if(width > 200 || height > 200){
           if(height > width){ // If Profil-orientated
                value = 0; 
                scale = 200/height;  //Factor to multiply Height and Width with
           }else{
                scale = 200/width;  
           }
            width =  (width * scale);
            height = (height * scale);
        }
        else{
            value = 3;  //if image is smaller than 200px
        }   
    
     
        
        
    // encode and save the image 
        AffineTransform at = new AffineTransform();

        switch(value){
           case 0: 
/*(3)*/    at.translate((height-width)/2,(width-height)/2);  //translates the rotated koordinates, so that the image is in the center again

           case 1: // rotate if needed = if profil orientation = height > width
/*(2)*/    at.rotate(Math.toRadians(90), width/2, height/2);  // rotatets Transform with 90degrees at the specified Point (width/2, Height/2)
            double temp = width;  //switch height and width for the new bufferedImages size 
            width = height;
            height = temp;

           case 2: // scale the image to a maximum of [ 200 w X 100 h ] pixels - do not distort! 
/*(1)*/    at.scale(scale,scale);  //will be applied first, because its specified last
            break;

           case 3: // if the image is smaller than [ 200 w X 100 h ] - print it on a [ dim X dim ] canvas!
            at.translate((100-(width/2)), (50-(height/2)));
            width = 200;
            height = 100; 
            break;
           default: break;
       }

        BufferedImage img = new BufferedImage((int)width,(int)height,original.getType()); 
        Graphics2D imgG2D = img.createGraphics();
        imgG2D.drawImage(original,at,null);
     
        ImageIO.write(img, "png", outputFile);
        
        return outputFile;

        /**
            ./ant.sh ImageThumbnailGenerator -Dinput=media/img/ -Doutput=test/ -Drotation=90
        */
    
    }

    /**
        Main method. Parses the commandline parameters and prints usage information if required.
    */
    public static void main( String[] args ) throws Exception
    {
        if ( args.length < 2 ) {
            System.out.println( "usage: java itm.image.ImageThumbnailGenerator <input-image> <output-directory>" );
            System.out.println( "usage: java itm.image.ImageThumbnailGenerator <input-directory> <output-directory>" );
            System.exit( 1 );
        }
        File fi = new File( args[0] );
        File fo = new File( args[1] );

        ImageThumbnailGenerator itg = new ImageThumbnailGenerator();
        itg.batchProcessImages( fi, fo, true );
    }    
}