package itm.video;

/*******************************************************************************
 This file is part of the ITM course 2017
 (c) University of Vienna 2009-2017
 *******************************************************************************/

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IContainerFormat;
import com.xuggle.xuggler.video.IConverter;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;
import com.xuggle.xuggler.video.ConverterFactory;
import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import java.awt.image.BufferedImage;

/**
 * 
 * This class creates JPEG thumbnails from from video frames grabbed from the
 * middle of a video stream It can be called with 2 parameters, an input
 * filename/directory and an output directory.
 * 
 * If the input file or the output directory do not exist, an exception is
 * thrown.
 */

public class VideoFrameGrabber {

	/**
	 * Constructor.
	 */
	public VideoFrameGrabber() {
	}

	/**
	 * Processes the passed input video file / video file directory and stores
	 * the processed files in the output directory.
	 * 
	 * @param input
	 *            a reference to the input video file / input directory
	 * @param output
	 *            a reference to the output directory
	 */
	public ArrayList<File> batchProcessVideoFiles(File input, File output) throws IOException {
		if (!input.exists())
			throw new IOException("Input file " + input + " was not found!");
		if (!output.exists())
			throw new IOException("Output directory " + output + " not found!");
		if (!output.isDirectory())
			throw new IOException(output + " is not a directory!");

		ArrayList<File> ret = new ArrayList<File>();

		if (input.isDirectory()) {
			File[] files = input.listFiles();
			for (File f : files) {
				if (f.isDirectory())
					continue;

				String ext = f.getName().substring(f.getName().lastIndexOf(".") + 1).toLowerCase();
				if (ext.equals("avi") || ext.equals("swf") || ext.equals("asf") || ext.equals("flv")
						|| ext.equals("mp4")) {
					File result = processVideo(f, output);
					System.out.println("converted " + f + " to " + result);
					ret.add(result);
				}

			}

		} else {
			String ext = input.getName().substring(input.getName().lastIndexOf(".") + 1).toLowerCase();
			if (ext.equals("avi") || ext.equals("swf") || ext.equals("asf") || ext.equals("flv") || ext.equals("mp4")) {
				File result = processVideo(input, output);
				System.out.println("converted " + input + " to " + result);
				ret.add(result);
			}
		}
		return ret;
	}

	/**
	 * Processes the passed audio file and stores the processed file to the
	 * output directory.
	 * 
	 * @param input
	 *            a reference to the input audio File
	 * @param output
	 *            a reference to the output directory
	 */
	protected File processVideo(File input, File output) throws IOException, IllegalArgumentException {
		if (!input.exists())
			throw new IOException("Input file " + input + " was not found!");
		if (input.isDirectory())
			throw new IOException("Input file " + input + " is a directory!");
		if (!output.exists())
			throw new IOException("Output directory " + output + " not found!");
		if (!output.isDirectory())
			throw new IOException(output + " is not a directory!");

		// ***************************************************************
		// Fill in your code here!
		// ***************************************************************

		File outputFile = new File(output, input.getName() + "_thumb.jpg");

		//https://github.com/artclarke/xuggle-xuggler/blob/master/src/com/xuggle/xuggler/demos/DecodeAndCaptureFrames.java
		
	
		IContainer container = null;
		IStream is = null;
		IStreamCoder videoCoder = null;
		int videoStreamId = 0;
		double timeBase = 0;

		
		// load the input video file
		container = IContainer.make();
		container.open(input.getAbsolutePath(), IContainer.Type.READ, null);

		//get the video stream (for there are several streams e.g. audio, discrete media etc)
		int streamNum = container.getNumStreams();
			
		for(int i = 0; i <= streamNum; i++) {
			is = container.getStream(i);  
			IStreamCoder tempCoder = is.getStreamCoder();
			//check if Decoder is for Video
			if(tempCoder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
				videoStreamId = i;
				videoCoder = tempCoder;
				break;
			}
		}

		videoCoder.open();

		//get to middle frame
		timeBase = is.getTimeBase().getDouble();
		long timeMs = container.getDuration() / 2000000; //milliseconds
		long timeMiddleFrame = (long) (timeMs/timeBase); //time of the seeked frame in the middle of vid
		container.seekKeyFrame(videoStreamId, timeMiddleFrame, IContainer.SEEK_FLAG_FRAME);
				
		//stream pixeltype may need to be converted with IVideoResembler
		IVideoResampler resampler = convertStream(videoCoder);

		IPacket packet = IPacket.make();
		
		//read the Packet 
		while(container.readNextPacket(packet) >= 0) {			
			if(packet.getStreamIndex() == videoStreamId) { // check if its video stream
				
				IVideoPicture pic = IVideoPicture.make(	videoCoder.getPixelType(), 
														videoCoder.getWidth(), 
														videoCoder.getHeight());
				int totalDecoded = 0;
				while(totalDecoded < packet.getSize()) {
					//decode video
					int decoded = videoCoder.decodeVideo(pic, packet, totalDecoded);
					totalDecoded += decoded;
				
					if(pic.isComplete()) {
						IVideoPicture newPic = pic;		
						//If Video Format is not BGR24, resample new VideoPicture
						if(resampler != null) {
							newPic = IVideoPicture.make(resampler.getOutputPixelFormat(),pic.getWidth(),pic.getHeight());
							resampler.resample(newPic, pic);
						}
						//convert VideoPicture to BufferedImage
						BufferedImage capture = new BufferedImage(	videoCoder.getWidth(), 
																	videoCoder.getHeight(), 
																	BufferedImage.TYPE_3BYTE_BGR );
						IConverter converter = ConverterFactory.createConverter(capture, newPic.getPixelType());
						capture = converter.toImage(newPic);

						//write image to outputFile
						ImageIO.write(capture,"jpg", outputFile);
					}
				}
				break;
			}
		}
	
		videoCoder.close();
		container.close();
		return outputFile;
	}

	public static IVideoResampler convertStream(IStreamCoder coder) {
		IVideoResampler resampler = null;
		if(coder.getPixelType() != IPixelFormat.Type.BGR24) {
			resampler =  IVideoResampler.make( 	coder.getWidth(),coder.getHeight(),IPixelFormat.Type.BGR24,
												coder.getWidth(),coder.getHeight(), coder.getPixelType()
												);		
		}
		return resampler;												 
	}

	/**
	 * Main method. Parses the commandline parameters and prints usage
	 * information if required.
	 */
	public static void main(String[] args) throws Exception {

		// args = new String[] { "./media/video", "./test" };

		if (args.length < 2) {
			System.out.println("usage: java itm.video.VideoFrameGrabber <input-videoFile> <output-directory>");
			System.out.println("usage: java itm.video.VideoFrameGrabber <input-directory> <output-directory>");
			System.exit(1);
		}
		File fi = new File(args[0]);
		File fo = new File(args[1]);
		VideoFrameGrabber grabber = new VideoFrameGrabber();
		grabber.batchProcessVideoFiles(fi, fo);
	}

}
